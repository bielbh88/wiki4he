# Wiki For HE

*Gabriel Cardoso Salgado, Nov-2018*

This repository contains a dataset for data analysis exercise and a jupyter notebook with study about this dataset analysis.

## Run

In order to run this study about data analysis, it is required follow some steps with installations.

For good practices, create these folders if not already created:
```bash
mkdir dev
mkdir downloads
```

Clone wiki4eh if not already cloned:
```bash
cd ~/dev
git clone https://[user]@bitbucket.org/bielbh88/wiki4eh.git
```

This project uses Python-3.6. Install it:
```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install python3.6
sudo apt-get install python3.6-dev
```

Now we have to install pip for this Python:
```bash
cd ~/downloads
wget https://bootstrap.pypa.io/get-pip.py
sudo -H python3.6 get-pip.py
```

Then install requirements:
```bash
cd ~/dev/wiki4he
sudo -H pip3 install -r requirements.txt
```

Maybe numpy need special care:
```bash
sudo apt-get install python3-numpy
sudo pip3 install numpy
```

And also pandas (get link at PyPI and replace version 0.22.0 by latest version):
```bash
sudo apt-get install python3-pandas
cd ~/downloads
wget https://pypi.python.org/packages/08/01/803834bc8a4e708aedebb133095a88a4dad9f45bbaf5ad777d2bea543c7e/pandas-0.22.0.tar.gz#md5=c7a2757b607748255f3270221ac61d59
tar -xzf pandas-0.22.0.tar.gz
cd pandas-0.22.0
sudo python3.4 setup.py build_ext --inplace
```

It is required install Jupyter too, because this study was done on a notebook:
```bash
pip3 install jupyter
```

Finally run jupyter:
```bash
cd ~/dev/wiki4he
jupyter notebook
```

And open notebook and run each block on jupyter interface.